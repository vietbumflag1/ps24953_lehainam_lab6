﻿CREATE DATABASE J5Shop
GO

USE J5Shop
GO

-- Tạo bảng Categories
CREATE TABLE Categories (
    id VARCHAR(255) PRIMARY KEY,
    name VARCHAR(255)
);

-- Tạo bảng Products
CREATE TABLE Products (
    id INT PRIMARY KEY IDENTITY,
    name VARCHAR(255),
    image VARCHAR(255),
    price FLOAT,
    Createdate DATE,
    available BIT,
    Categoryid VARCHAR(255),
    FOREIGN KEY (Categoryid) REFERENCES Categories(id)
);

-- Tạo bảng Accounts
CREATE TABLE Accounts (
    username VARCHAR(255) PRIMARY KEY,
    password VARCHAR(255),
    fullname VARCHAR(255),
    email VARCHAR(255),
    photo VARCHAR(255),
    activated BIT,
    admin BIT
);

-- Tạo bảng Orders
CREATE TABLE Orders (
    id BIGINT PRIMARY KEY IDENTITY,
    address VARCHAR(255),
    Createdate DATE,
    Username VARCHAR(255),
    FOREIGN KEY (Username) REFERENCES Accounts(username)
);

-- Tạo bảng Orderdetails
CREATE TABLE Orderdetails (
    id BIGINT PRIMARY KEY IDENTITY,
    price FLOAT,
    quantity INT,
    Productid INT,
    Orderid BIGINT,
    FOREIGN KEY (Productid) REFERENCES Products(id),
    FOREIGN KEY (Orderid) REFERENCES Orders(id)
);


