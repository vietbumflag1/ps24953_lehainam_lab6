package com.poly.utils;

import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Service;

@Service
public class SessionService {

    private HttpSession session;

    public SessionService(HttpSession session) {
        this.session = session;
    }

    public void set(String key, Object value) {
        session.setAttribute(key, value);
    }

    public <T> T get(String key, T defaultValue) {
        Object value = session.getAttribute(key);
        if (value != null) {
            return (T) value;
        }
        return defaultValue;
    }
}

