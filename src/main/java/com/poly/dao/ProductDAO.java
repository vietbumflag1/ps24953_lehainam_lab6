package com.poly.dao;

import com.poly.entity.Product;
import com.poly.entity.Report;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductDAO extends JpaRepository<Product, Integer> {
//    @Query("FROM Product WHERE price BETWEEN ?1 AND ?2")
//    List<Product> findByPrice(double minPrice, double maxPrice);

    List<Product> findByPriceBetween(double minPrice, double maxPrice);

    @Query("FROM Product WHERE name LIKE ?1")
    Page<Product> findByKeywords(String keywords, Pageable pageable);

    Page<Product> findAllByNameLike(String keywords, Pageable pageable);

    @Query("SELECT new com.vudinhquy.lab5.entity.Report(o.category, sum(o.price), count(o))"
            + "FROM Product o "
            + "GROUP BY o.category "
            + "ORDER BY sum(o.price) DESC")
    List<Report> getInventoryByCategory();
}
