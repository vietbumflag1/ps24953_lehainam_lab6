package com.poly.entity;

import lombok.*;

@Data
@NoArgsConstructor
public class Report {
    private Category category;
    private Double sum;
    private Long count;

    public Report(Category category, Double sum, Long count) {
        this.category = category;
        this.sum = sum;
        this.count = count;
    }
}

