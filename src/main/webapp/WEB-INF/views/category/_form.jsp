<%--
  Created by IntelliJ IDEA.
  User: LT
  Date: 5/30/2023
  Time: 1:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Title</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
</head>
<body>
    <form:form action="/category/index" modelAttribute="item" class="form">
        <div class="mb-3">
            <label for="id" class="form-label">Category Id</label>
            <form:input path="id" placeholder="Category Id?" class="form-control" id="id" />
        </div>
        <div class="mb-3">
            <label for="name" class="form-label">Category Name</label>
            <form:input path="name" placeholder="Category Name?" class="form-control" id="name" />
        </div>
        <hr>
        <button formaction="/category/create" class="btn btn-success">Create</button>
        <button formaction="/category/update" class="btn btn-primary">Update</button>
        <a href="/category/delete/${item.id}" class="btn btn-danger">Delete</a>
        <a href="/category/index" class="btn btn-secondary">Reset</a>
    </form:form>
</body>
</html>
