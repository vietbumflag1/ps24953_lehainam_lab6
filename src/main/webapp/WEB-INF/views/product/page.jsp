<%--
  Created by IntelliJ IDEA.
  User: LT
  Date: 5/30/2023
  Time: 4:21 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<html>
<head>
    <title>Pagination</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
            crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <h3>PAGINATION</h3>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Price</th>
                <th>Date</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="item" items="${page.content}">
                <tr>
                    <td>${item.id}</td>
                    <td>${item.name}</td>
                    <td>${item.price}</td>
                    <td>${item.createDate}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <nav aria-label="Page navigation">
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" style="text-decoration: none" href="/product/page?p=0">First</a>
                </li>
                <li class="page-item">
                    <a class="page-link" style="text-decoration: none" href="/product/page?p=${page.number-1}">Previous</a>
                </li>
                <li class="page-item">
                    <a class="page-link" style="text-decoration: none" href="/product/page?p=${page.number+1}">Next</a>
                </li>
                <li class="page-item">
                    <a class="page-link" style="text-decoration: none" href="/product/page?p=${page.totalPages-1}">Last</a>
                </li>
            </ul>
        </nav>
        <ul>
            <li>Số thực thể hiện tại: ${page.numberOfElements}</li>
            <li>Trang số: ${page.number}</li>
            <li>Kích thước trang: ${page.size}</li>
            <li>Tổng số thực thể: ${page.totalElements}</li>
            <li>Tổng số trang: ${page.totalPages}</li>
        </ul>
    </div>
</body>
</html>
